CREATE TABLE IF NOT EXISTS timezone
(
    `id`       BIGINT AUTO_INCREMENT PRIMARY KEY,
    `zoneId`   VARCHAR(64) NOT NULL,
    `staff_id` BIGINT,
    FOREIGN KEY (`staff_id`) REFERENCES staff (`id`)
)