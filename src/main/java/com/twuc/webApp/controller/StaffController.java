package com.twuc.webApp.controller;

import com.twuc.webApp.domain.*;
//TODO: unused imports
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/staffs")
public class StaffController {
    private final TimezoneRepository timezoneRepository;

    private final StaffRepository staffRepository;

    public StaffController(TimezoneRepository timezoneRepository, StaffRepository staffRepository) {
        this.timezoneRepository = timezoneRepository;
        this.staffRepository = staffRepository;
    }

    @PostMapping("")
    public ResponseEntity<Object> AddStaff(@Valid @RequestBody PostStaffRequest postStaffRequest) {
        Staff staff = new Staff(postStaffRequest.getFirstName(), postStaffRequest.getLastName());
        //TODO: better not use repository in controller
        staffRepository.saveAndFlush(staff);
        return ResponseEntity
                .status(HttpStatus.OK)
                .header("Location", "/api/staffs/" + staff.getId()).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<GetStaffResponse> getStaff(@PathVariable Long id) {
        Staff staff = staffRepository.getOne(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new GetStaffResponse(staff));
    }

    @GetMapping("")
    public ResponseEntity<List<Staff>> getAllStaff(){
        List<Staff> allStaff = staffRepository.findAll();
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(allStaff);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
    }

    @PutMapping("/{id}/timezone")
    @ResponseStatus(HttpStatus.OK)
    public void updateTimezone(@PathVariable Long id, @Valid @RequestBody UpdateTimezoneRequest updateTimezoneRequest) {
        Staff staff = staffRepository.getOne(id);
        Timezone timezone = new Timezone(updateTimezoneRequest.getZoneId());
        staff.setTimezone(timezone);
        timezoneRepository.saveAndFlush(timezone);
    }
}
