package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;
import java.util.Set;

@RestController
public class TimezoneController {
    @GetMapping("/api/timezones")
    public ResponseEntity<Set<String>> getTimezones() {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(availableZoneIds);
    }
}
