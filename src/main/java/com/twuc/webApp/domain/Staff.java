package com.twuc.webApp.domain;

import javax.persistence.*;
import java.time.zone.ZoneRulesProvider;
import java.util.Set;

@Entity
public class Staff {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false, length = 64)
    private String firstName;
    @Column(nullable = false, length = 64)
    private String lastName;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "staff")
    private Timezone timezone;

    public Staff() {
    }

    public Staff(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setTimezone(Timezone timezone) {
        this.timezone = timezone;
    }

    public Timezone getTimezone() {
        return timezone;
    }
}
