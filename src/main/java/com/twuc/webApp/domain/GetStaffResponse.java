package com.twuc.webApp.domain;

import java.util.Objects;

public class GetStaffResponse {
    private String firstName;
    private String lastName;
    private String zoneId;

    public GetStaffResponse(Staff staff, Timezone timezone) {
        Objects.requireNonNull(staff);

        this.firstName = staff.getFirstName();
        this.lastName = staff.getLastName();
        this.zoneId = timezone.getZoneId();
    }


    public GetStaffResponse(Staff staff) {
        Objects.requireNonNull(staff);
        this.firstName = staff.getFirstName();
        this.lastName = staff.getLastName();
        this.zoneId = staff.getTimezone().getZoneId();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getZoneId() {
        return zoneId;
    }
}
