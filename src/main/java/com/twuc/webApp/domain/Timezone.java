package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Timezone {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String zoneId;
    @OneToOne
    private Staff staff;

    public Timezone(String zoneId) {
        this.zoneId = zoneId;
    }

    public Timezone() {
    }

    public Long getId() {
        return id;
    }

    public String getZoneId() {
        return zoneId;
    }

    public Staff getStaff() {
        return staff;
    }
}
