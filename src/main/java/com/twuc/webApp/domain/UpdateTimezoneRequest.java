package com.twuc.webApp.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UpdateTimezoneRequest {
    @NotNull
    //TODO: 需求没看清，PT{number}H
    @Pattern(regexp = "(^[A-Z]\\w+)/([A-Z]\\w+)")
    private String zoneId;

    public UpdateTimezoneRequest() {
    }

    public UpdateTimezoneRequest(
    @NotNull
    //TODO: 如果用了TDD或者遵守了YAGNI原则，constructor一般不会出现annotation
    @Pattern(regexp = "(^[A-Z]\\w+)/([A-Z]\\w+)") String zoneId) { this.zoneId = zoneId;
    }

    public String getZoneId() {
        return zoneId;
    }
}
