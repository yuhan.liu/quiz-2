package com.twuc.webApp.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//TODO: bad naming
public class PostStaffRequest {
    @NotNull
    @Size(max = 64)
    private String firstName;
    @NotNull
    @Size(max = 64)
    private String lastName;

    public PostStaffRequest() {
    }


    //TODO: 如果用了TDD或者遵守了YAGNI原则，constructor一般不会出现annotation
    public PostStaffRequest(@NotNull @Size(max = 64) String firstName, @NotNull @Size(max = 64) String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
