package com.twuc.webApp.controller;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.domain.GetStaffResponse;
import com.twuc.webApp.domain.Staff;
import com.twuc.webApp.domain.Timezone;
import com.twuc.webApp.domain.UpdateTimezoneRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


class StaffControllerTest extends ApiTestBase {

    private Staff staff = new Staff("liu", "yuHan");

    @Test
    void could_add_staff() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staff)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(header().string("Location", "/api/staffs/1"));
    }

    @Test
    void should_get_staff() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staff)));

        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("[{\"id\":1,\"firstName\":\"liu\",\"lastName\":\"yuHan\",\"timezone\":null}]"));
    }

    @Test
    void should_get_all_staff() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staff)));
        Staff otherStaff = new Staff("liu", "yu");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(otherStaff)));

        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("[{\"id\":1,\"firstName\":\"liu\",\"lastName\":\"yuHan\",\"timezone\":null},{\"id\":2,\"firstName\":\"liu\",\"lastName\":\"yu\",\"timezone\":null}]"));
    }

    @Test
    void could_return_error_when_staff_message_is_invalid() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Staff(null, "yuHan"))))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_staff_which_contains_zoneId() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staff)));

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new UpdateTimezoneRequest("Asia/Chongqing"))));

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(serialize(new GetStaffResponse(staff, new Timezone("Asia/Chongqing")))));
    }
}