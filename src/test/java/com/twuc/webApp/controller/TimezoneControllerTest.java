package com.twuc.webApp.controller;

import com.twuc.webApp.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.time.zone.ZoneRulesProvider;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

class TimezoneControllerTest extends ApiTestBase {
    @Test
    void should_get_list_for_all_zoneId() throws Exception {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        mockMvc.perform(get("/api/timezones"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string(serialize(availableZoneIds)));
    }
}