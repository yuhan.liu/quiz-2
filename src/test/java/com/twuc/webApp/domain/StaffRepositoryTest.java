package com.twuc.webApp.domain;

import com.twuc.webApp.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class StaffRepositoryTest extends ApiTestBase {
    @Autowired
    private StaffRepository staffRepository;

    @Test
    void should_input_staff_in_table() {
        Staff staff = new Staff("liu","yuHan");
        staffRepository.saveAndFlush(staff);
        Staff staff1 = staffRepository.getOne(staff.getId());
        assertNotNull(staff1);
    }
}