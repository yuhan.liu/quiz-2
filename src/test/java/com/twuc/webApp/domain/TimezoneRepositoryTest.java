package com.twuc.webApp.domain;

import com.twuc.webApp.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class TimezoneRepositoryTest extends ApiTestBase {
    @Autowired
    private StaffRepository staffRepository;

    @Test
    void should_save_timezone_when_save_staff() {
        Staff staff = new Staff("liu", "yuHan");
        Timezone timezone = new Timezone("Asia/Chongqing");
        staff.setTimezone(timezone);
        staffRepository.saveAndFlush(staff);
        Staff savedStaff = staffRepository.getOne(staff.getId());
        assertNotNull(savedStaff.getTimezone());
    }
}